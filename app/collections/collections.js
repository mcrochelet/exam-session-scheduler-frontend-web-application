/**
 * Collections definition both for the server and the client (miniMongo Collections)
 * @type {Meteor.Collection}
 */

Instances = new Meteor.Collection('instances');
Students = new Meteor.Collection('students');
Courses = new Meteor.Collection('courses');
Professors = new Meteor.Collection('professors');
Constraints = new Meteor.Collection('constraints');

BulkFiles = new Meteor.Collection('bulk-files');
Results = new Meteor.Collection('results');