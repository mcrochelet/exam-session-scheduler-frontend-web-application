var searchText = "";
var searchTextDep = new Tracker.Dependency;

var position = "";
var positionDep = new Tracker.Dependency;

var optionsChecked = {};
var optionsCheckedDep = new Tracker.Dependency;

var events = [];
var eventsDep = new Tracker.Dependency;


var slots = {
    2: [
        {
            start:{hour: 8, minute: 30},
            end: {hour: 12, minute: 30}
        },{
            start:{hour: 14, minute: 0},
            end: {hour: 18, minute: 0}
        }
    ],
    3: [
        {
            start:{hour: 8, minute: 30},
            end: {hour: 12, minute: 30}
        },{
            start:{hour: 14, minute: 0},
            end: {hour: 16, minute: 0}
        },{
            start:{hour: 16, minute: 0},
            end: {hour: 18, minute: 0}
        }
    ],
    4: [
        {
            start:{hour: 8, minute: 30},
            end: {hour: 10, minute: 30}
        },{
            start:{hour:10, minute: 30},
            end: {hour: 12, minute: 30}
        },{
            start:{hour: 14, minute: 0},
            end: {hour: 16, minute: 0}
        },{
            start:{hour: 16, minute: 0},
            end: {hour: 18, minute: 0}
        }, {
            marinaSargsyanJetAime: 25
        }
    ]
};

Template.results.helpers({
    matchingInstances: function() {
        searchTextDep.depend();
        var currentInstance = Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()});
        var regex = ".*"+searchText+".*";
        var matches = Instances.find({$and: [{$or: [{year: {$regex: regex}}, {month: {$regex: regex}}]}, {userId: Meteor.userId()}]}).fetch();
        if (currentInstance) matches = _.filter(matches, function(doc) {return (doc.year !== currentInstance.year)
            || (doc.month !== currentInstance.month)});
        var groupedResults = _(matches).groupBy(function(doc) {return doc.year});
        groupedResults = _(groupedResults).pairs();

        groupedResults = _(groupedResults).map(function(array) {
            var docs = _.map(array[1], function(doc) {return {month: doc.month, _id: doc._id}});
            return {
                year: array[0],
                docs: docs
            }
        });
        return _(groupedResults).sortBy(function(doc) {return doc.year});

    },
    currentInstance: function() {
        return Instances.findOne({_id: Session.get('currentInstance')});
    }
});

Template.results.events({
    'keyup #search': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200)
});

var once = true;

Template.displayCalendar.helpers({
    options: function () {
        var cinst = Instances.findOne({_id: Session.get('currentInstance')});
        var h = $("#instanceDetails").height();
        h = (h)? h : 700;
        return {
            height: h,
            defaultView: 'month',
            firstDay: 1,

            header: {
                left: 'title',
                center: '',
                right:  'agendaDay,agendaWeek,month, prev,next'
            },
            events: function(start, end, timezone, cb) {
                cb(events)
            },
            viewRender: function() {
                if (once){
                    $('.fc').fullCalendar('gotoDate', moment(cinst.start));
                    once = false;
                }
            }
        }
    },
    selectors: function() {
        var f = {};

        var s = Students.findOne({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});

        var dValues = [];
        if (s) {
            var sKeys = _.map(
                _.filter(
                    _.keys(s),
                    function(str) {
                        return str.indexOf('stud')>-1 && str !== 'student';
                    }),
                function(str) {
                    f[str] = 1;
                    return str.replace('stud', '')
                });


            var students = Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}
                , {fields: f}).fetch();
            if (students && students.length >1)
                _.map(sKeys, function(k) {
                    var key = 'stud'+k;
                    var dValuesFork = _.pluck(_.uniq(students, false, function(s){
                        return s[key]}), key);
                    dValues.push({name: k, values: _.map(_.filter(dValuesFork, function(val) {return val !== ""}), function(v){
                        return {val: v}
                    })});
                });
        }
        return dValues;
    },
    // allows this computation to only run when the template is displayed and builds on the meteor auto-detection of
    // data reactive dependencies (updates the calendar)
    fakeHelper : function() {
        optionsCheckedDep.depend();
        var query = {$or: []};
        var options = _.keys(optionsChecked);

        events = [];
        var sol = Results.findOne({instanceId: Session.get('currentInstance')});

        var cinst = Instances.findOne({_id: Session.get('currentInstance')});

        if (cinst && cinst.start && cinst.nSessionsPerDay && sol && sol.results){

            var nSessPerDay = cinst.nSessionsPerDay;
            var results = sol.results;

            var courseTags = _.keys(results);

            _.map(options, function(option) {

                _.map(optionsChecked[option], function(opt) {
                    var q = {};
                    var key = 'stud'+option;
                    q[key] = opt;
                    query['$or'].push(q)

                })
            });
            if (query['$or'].length > 0) {
                var allowedCoursesIds = _.uniq(_.flatten(_.map(Students.find(query, {fields: {follows: 1}}).fetch(), function(s){
                    return s.follows
                })));
                var tmp = [];
                _.map(Courses.find({_id: {$in: allowedCoursesIds}}, {fields: {tag: 1}}).fetch(), function(c) {
                    if (!_.contains(tmp, c.tag)) tmp.push(c.tag);
                });
                courseTags = tmp
            }




            _.map(courseTags, function(courseTag){
                var sessions = results[courseTag];
                var numberOfDaysFromStart = _.map(sessions, function(session) {
                    return Math.floor(session / nSessPerDay);
                });
                var sessionInDay = _.map(sessions, function(session) {
                    return session % nSessPerDay;
                });

                for (var i=0; i<sessions.length; i++) {
                    events.push({
                        title: courseTag,
                        start: moment(cinst.start).add({
                            days: numberOfDaysFromStart[i],
                            hours: slots[nSessPerDay][sessionInDay[i]].start.hour,
                            minutes:slots[nSessPerDay][sessionInDay[i]].start.minute
                        }),
                        end: moment(cinst.start).add({
                            days: numberOfDaysFromStart[i],
                            hours: slots[nSessPerDay][sessionInDay[i]].end.hour,
                            minutes:slots[nSessPerDay][sessionInDay[i]].end.minute
                        }),
                        allDay: false
                    })
                }
            });
            $('.fc').fullCalendar( 'refetchEvents' );

            eventsDep.changed()
        }
    }

});

Template.displayCalendar.events({
    'click input': function(event) {
        var checked = $(event.target).is(":checked");
        var optionName = $(event.target).parents('.option-container').find('h4').html();
        var cinst = Instances.findOne({_id: Session.get('currentInstance')});

        var optionChecked = $(event.target).val();
        if (checked) {
            if (optionsChecked[optionName])
                optionsChecked[optionName].push(optionChecked);
            else
                optionsChecked[optionName] = [optionChecked]
        } else {
            var courseIndex = optionsChecked[optionName].indexOf(optionChecked);
            if (courseIndex > -1) optionsChecked[optionName].splice(courseIndex, 1)
        }
        $('.fc').fullCalendar('gotoDate', moment(cinst.start));
        optionsCheckedDep.changed()
    }
});