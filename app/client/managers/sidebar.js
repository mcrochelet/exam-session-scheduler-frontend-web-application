/**
 * Created by mcrochelet on 21/05/15.
 */
Template.sidebar.helpers({
    firstPage: function() {
        return Router.current().route.getName() === 'landingPage'
    }
});

Template.sidebar.events = {
    'click a': function() {
        if ($('.ui.sidebar').sidebar('is visible')) {
            $('.ui.sidebar').sidebar('pull page')
        }
    }
};
