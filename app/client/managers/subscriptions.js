/**
 * Basically always subscribe each client to all the data he can
 */
Meteor.subscribe('instances');
Meteor.subscribe('students');
Meteor.subscribe('courses');
Meteor.subscribe('professors');
Meteor.subscribe('constraints');
Meteor.subscribe('bulk-files');
Meteor.subscribe('results');
