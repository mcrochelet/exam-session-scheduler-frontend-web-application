var resourcePosition = 0;
var resourcePositionDep = new Tracker.Dependency();

var nResourcesPerPage = 20;

var searchText = "";
var searchTextDep = new Tracker.Dependency;

Template.displayCoursesOfInstance.helpers({
    editing: function() {
        return Session.get('editing');
    },
    courses: function() {
        resourcePositionDep.depend();
        searchTextDep.depend();
        var regex = ".*"+searchText+".*";

        return Courses.find(
            {$and: [
                {$or: [
                    {name: {$regex: regex}},
                    {tag: {$regex: regex}}]},
                {instanceId: Session.get('currentInstance')},
                {userId: Meteor.userId()}
            ]},
            {skip: resourcePosition, limit: resourcePosition+nResourcesPerPage, sort: {tag: 1, name: 1}})
    },
    nCourses: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    },
    st: function() {
        resourcePositionDep.depend();
        return resourcePosition
    },
    en: function() {
        resourcePositionDep.depend();
        return resourcePosition + nResourcesPerPage
    }
});

Template.displayCoursesOfInstance.events({
    'click #addCourseRow': function() {
        var $template = $('.editionRow[courseEdition]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#name").val("");
        $newLine.find("#tag").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click .remResourceRow': function() {
        var self= this;
        Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).forEach(function(student) {
            Students.update({_id: student._id}, {$pull: {follows: self._id}})
        });
        Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).forEach(function(professor) {
            Professors.update({_id: professor._id}, {$pull: {teaches: self._id}})
        });
        Constraints.find({instanceId: Session.get('currentInstance'), courseId: self._id, userId: Meteor.userId()}).forEach(function(constraint) {
            Constraints.remove({_id: constraint._id});
        });
        Courses.remove({_id: this._id});
    },
    'click .remEditionRow': function(e) {
        if ($('.editionRow[courseEdition]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        }
    },
    'click #submitCourses': function() {
        var self = this;

        _.map($('.resource-row[course]'), function(e){
            if ($(e).find('#name').val() !== "" && $(e).find('#tag').val() !== "")
                Courses.update({_id: $(e).attr('appId')}, {
                    name: $(e).find('#name').val(),
                    tag: $(e).find('#tag').val(),
                    instanceId: self._id,
                    userId: Meteor.userId()
                });
        });

        _.map($('.resource-row[courseEdition]'), function(e){
            if ($(e).find('#name').val() !== "" && $(e).find('#tag').val() !== ""){
                var candidate = {
                    name: $(e).find('#name').val(),
                    tag: $(e).find('#tag').val(),
                    instanceId: self._id,
                    userId: Meteor.userId()
                };
                if (!Courses.findOne(candidate))Courses.insert(candidate);
            }
        });



        _.map($('.editionRow[courseEdition]'), function(e) {

            if ($('.editionRow[courseEdition]').length > 1)
                $(e).remove();
            else {
                $(e).find('#name').val("");
                $(e).find('#tag').val("");
            }
        });
    },
    'click #loadNext': function() {
        resourcePosition = Math.min(
            Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count() - nResourcesPerPage,
            resourcePosition + nResourcesPerPage);
        resourcePositionDep.changed()
    },
    'click #loadPrevious': function() {
        resourcePosition = Math.max(0,
            resourcePosition - nResourcesPerPage);
        resourcePositionDep.changed()
    },
    'keyup #searchResource': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200)
});

Template.displayCourseOfInstance.helpers({
    disabled: function() {
        return (Session.get('editing')) ? "" : "disabled";
    }
});