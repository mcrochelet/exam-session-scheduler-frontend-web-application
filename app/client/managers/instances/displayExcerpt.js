Template.displayExcerpt.rendered = function() {
    var currentInstance = Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()});
    var datepickerOptions = {
        format: 'D dd MM',
        defaultViewDate: {
            month: currentInstance.month === "January" ? 0
                : (currentInstance.month === "June" ? 5 : 7)
        }
    };
    $('.datepicker').datepicker(datepickerOptions);
};

Template.displayExcerpt.helpers({
    editing: function() {
        return Session.get('editing');
    },
    disabled: function(){
        return Session.get('editing')? "" : "disabled";
    },
    currentInstance: function() {
        return Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()})
    },
    startingDay: function() {
        var date = Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()}).start;
        $('#startingDay').datepicker('update', date);
        return moment(date).format('ddd DD MMMM');
    },
    endingDay: function() {
        var date = Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()}).end;
        $('#endingDay').datepicker('update', date);
        return moment(date).format('ddd DD MMMM');

    },
    nSessionsPerDay: function() {
        return Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()}).nSessionsPerDay;
    },
    nStudents: function() {
        return Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    },
    nCourses: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    },
    nProfs: function() {
        return Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    },
    nConst: function() {
        return Constraints.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    },
    avgStudPerC: function() {
        var nStud = Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();;
        var sum = 0;
        Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {follows: 1}}).forEach(function(a) {
            sum += a.follows.length;
        });

        return sum !== 0 ? Math.round(sum / nStud*100)/100 : 0;
    },
    avgProfPerC: function() {
        var nProfs = Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();;
        var sum = 0;
        Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {teaches: 1}}).forEach(function(a) {
            sum += a.teaches.length;
        });

        return sum !== 0 ? Math.round(sum / nProfs*100)/100 : sum;
    },
    displayAlert: function() {
        return Instances.findOne({_id: Session.get('currentInstance')}).status !== 'idle';
    }
});

Template.displayExcerpt.events({
    'click #submitDates': function(){
        var $sDay =$('#startingDay');
        var $eDay =$('#endingDay');
        var $nSess= $('#nSessionsPerDay');
        var d1 = $sDay.datepicker('getDate');
        var d2 = $eDay.datepicker('getDate');
        var nSessionsPerDay = parseInt($nSess.val());

        if (moment(d1).isBefore(moment(d2))) {
            if (d1 instanceof Date && d2 instanceof Date && !isNaN(nSessionsPerDay))
                Instances.update({_id: Session.get('currentInstance')}, {$set: {
                    start: d1,
                    end: d2,
                    nSessionsPerDay: nSessionsPerDay
                }});

            var cinst = Instances.findOne({_id: Session.get('currentInstance')});

            $sDay.val(moment(cinst.start).format('ddd DD MMMM'));
            $eDay.val(moment(cinst.end).format('ddd DD MMMM'));
            $nSess.val(cinst.nSessionsPerDay);
        } else {
            $eDay.val('Must be after start!')
        }

    },
    'click #solve': function() {
        var cinst = Instances.findOne({_id: Session.get('currentInstance')});
        var nStud = Students.find({instanceId: cinst._id}).count();
        var nProf = Professors.find({instanceId: cinst._id}).count();

        var maxTime = parseInt($('#maxTime').val());
        maxTime = (isNaN(maxTime))? 5 : maxTime;
        if (cinst.start && cinst.end && cinst.nSessionsPerDay && nStud > 0 && nProf > 0) {
            Meteor.call('solve', Session.get('currentInstance'), maxTime);
            $('#solvingModal').modal('hide');
            Router.go('/statistics')
        }
    }
});