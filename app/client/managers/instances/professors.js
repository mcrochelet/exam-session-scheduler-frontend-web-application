var resourcePosition = 0;
var resourcePositionDep = new Tracker.Dependency();

var nResourcesPerPage = 20;

var searchText = "";
var searchTextDep = new Tracker.Dependency;

Template.displayProfessorsOfInstance.helpers({
    editing: function() {
        return Session.get('editing');
    },
    professors: function() {
        resourcePositionDep.depend();
        searchTextDep.depend();
        var regex = ".*"+searchText+".*";

        return Professors.find(
            {$and: [
                {$or: [
                    {name: {$regex: regex}},
                    {surname: {$regex: regex}}]},
                {instanceId: Session.get('currentInstance')},
                {userId: Meteor.userId()}
            ]},
            {skip: resourcePosition, limit: resourcePosition+nResourcesPerPage, sort: {name: 1, surname: 1}})
    },
    nProfs: function() {
        return Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    },
    st: function() {
        resourcePositionDep.depend();
        return resourcePosition
    },
    en: function() {
        resourcePositionDep.depend();
        return resourcePosition + nResourcesPerPage
    }
});

Template.displayProfessorsOfInstance.events({
    'click #addProfessorRow': function() {
        var $template = $('.editionRow[professorEdition]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#name").val("");
        $newLine.find("#surname").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click .remResourceRow': function() {
        Constraints.find({instanceId: Session.get('currentInstance'), profId: this._id, userId: Meteor.userId()}).forEach(function(constraint) {
            Constraints.remove({_id: constraint._id});
        });
        Professors.remove({_id: this._id});
    },
    'click .remEditionRow': function(e) {
        if ($('.editionRow[professorEdition]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        }
    },
    'click #submitProfessors': function() {
        var self = this;

        _.map($('.resource-row[professor]'), function(e){
            if ($(e).find('#name').val() !== "" && $(e).find('#surname').val() !== "")
                Professors.update({_id: $(e).attr('appId')}, {
                    name: $(e).find('#name').val(),
                    surname: $(e).find('#surname').val(),
                    instanceId: self._id,
                    userId: Meteor.userId()
                });
        });

        _.map($('.resource-row[professorEdition]'), function(e){
            if ($(e).find('#name').val() !== "" && $(e).find('#surname').val() !== ""){
                var candidate = {
                    name: $(e).find('#name').val(),
                    surname: $(e).find('#surname').val(),
                    instanceId: self._id,
                    userId: Meteor.userId()
                };
                if (!Professors.findOne(candidate)) Professors.insert(candidate)
            }
        });

        _.map($('.editionRow[professorEdition]'), function(e) {

            if ($('.editionRow[professorEdition]').length > 1)
                $(e).remove();
            else {
                $(e).find('#name').val("");
                $(e).find('#surname').val("");
            }
        });
    }
    ,
    'click #loadNext': function() {
        resourcePosition = Math.min(
            Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count() - nResourcesPerPage,
            resourcePosition + nResourcesPerPage);
        resourcePositionDep.changed()
    },
    'click #loadPrevious': function() {
        resourcePosition = Math.max(0,
            resourcePosition - nResourcesPerPage);
        resourcePositionDep.changed()
    },
    'keyup #searchResource': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200)
});

Template.displayProfessorOfInstance.helpers({
    disabled: function() {
        return (Session.get('editing')) ? "" : "disabled";
    }
});