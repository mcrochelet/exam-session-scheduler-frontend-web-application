var page = new ReactiveVar(0);
var file = {};
var fileDep = new Tracker.Dependency();

var fileHeaders = {};
var fileHeadersDep = new Tracker.Dependency();
var exampleRow = {};
var exampleRowDep = new Tracker.Dependency();

var pageNames = [
    "Excerpt",
    "Students",
    "Courses",
    "Professors",
    "Enrolments",
    "Teachings",
    "Constraints"
];

Template.displayInstance.helpers({
    isFirstPage: function() {
        return page.get() === 0
    },
    isLastPage: function() {
        return page.get() === 6
    },
    previousPageName: function() {
        return pageNames[Math.max(0, page.get()-1)]
    },
    nextPageName: function() {
        return pageNames[Math.min(6, page.get()+1)]
    },
    correctFile: function() {
        fileHeadersDep.depend();

        var student = _(fileHeaders).contains('student');
        var prof = _(fileHeaders).contains('profs');
        var course = _(fileHeaders).contains('course');
        var inscribed = _(fileHeaders).contains('inscribed');

        return student && prof && course && inscribed
    },
    missingStudent: function(){
        fileHeadersDep.depend();
        return !_(fileHeaders).contains('student');
    },
    missingProfs: function(){
        fileHeadersDep.depend();
        return !_(fileHeaders).contains('profs')
    },
    missingCourse: function(){
        fileHeadersDep.depend();
        return !_(fileHeaders).contains('course')
    },
    missingInscribed: function(){
        fileHeadersDep.depend();
        return !_(fileHeaders).contains('inscribed')
    },
    headers: function() {
        exampleRowDep.depend();
        return JSON.stringify(_.omit(exampleRow, 'null'), null, 4).split('{').join('').split('}').join('');
    }
});

Template.displayInstance.rendered = function() {
    prettyPrint();
};

Template.displayInstance.events({
    'click #gotoPrevious': function(){
        var $fromto = $('#fromto');
        $fromto.attr('from', page.get());
        $fromto.attr('to', Math.max(0, page.get()-1));
        page.set(Math.max(0, page.get()-1))
    },
    'click #gotoNext': function(){
        var $fromto = $('#fromto');
        $fromto.attr('from', page.get());
        $fromto.attr('to', Math.min(6, page.get()+1));
        page.set(Math.min(6, page.get()+1))
    },
    'change #fileUpload': function(evt) {
        file = evt.target.files[0];
        fileDep.changed();

        Papa.parse(file, {
            header: true,
            preview: 0,
            complete: function(results) {
                fileHeaders = results.meta.fields;
                exampleRow = results.data[0];
                fileHeadersDep.changed();
                exampleRowDep.changed();
                $('#parsingModal').modal("show");
            }
        });

    },
    'click #launchAnalysis': function() {
        var instanceId = Session.get('currentInstance');
        Papa.parse(file, {
            header: true,
            complete: function(results) {
                BulkFiles.insert({
                    userId: Meteor.userId(),
                    instanceId: instanceId,
                    results: results,
                    status: 'to-parse'
                });
            }
        });
    }
});

Template.displayInstanceDetailsPage.helpers({
    isFirstPage: function() {
        return page.get() === 0
    },
    isStudentPage: function() {
        return page.get() === 1
    },
    isCoursePage: function() {
        return page.get() === 2
    },
    isProfessorPage: function() {
        return page.get() === 3
    },
    isSFCPage: function() {
        return page.get() === 4
    },
    isPTCPage: function() {
        return page.get() === 5
    },
    isConstraintPage: function() {
        return page.get() === 6
    }
});

