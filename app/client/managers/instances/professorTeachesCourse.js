var resourcePosition = 0;
var resourcePositionDep = new Tracker.Dependency();

var nResourcesPerPage = 20;

var searchText = "";
var searchTextDep = new Tracker.Dependency;

Template.displayProfessorTeachesCoursesOfInstance.helpers({
    editing: function() {
        return Session.get('editing');
    },
    links: function() {
        var results = [];

        resourcePositionDep.depend();
        searchTextDep.depend();
        var regex = ".*"+searchText+".*";

        var nProfs = Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();;
        var sum = 0;
        Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {teaches: 1}}).forEach(function(a) {
            sum += a.teaches.length;
        });
        var avgenrol = Math.round(sum / nProfs);

        var stepper = Math.round(nResourcesPerPage /avgenrol);

        _.map(Professors.find({$and: [
            {$or: [
                {name: {$regex: regex}},
                {surname: {$regex: regex}}]},
            {instanceId: Session.get('currentInstance')},
            {userId: Meteor.userId()}
        ]},  {
            skip: resourcePosition,
            limit: resourcePosition+stepper,
            sort: {name: 1, surname: 1}}).fetch(), function(s) {
            if (s && s.teaches && s.name) _.map(s.teaches, function(cId) {
                var course = Courses.findOne({_id: cId});
                if (course) results.push({
                    sId: s._id,
                    name: s.name,
                    surname:s.surname,
                    cId: course._id,
                    courseTag: course.tag,
                    courseName: course.name
                });
            });
        });
        return results
    },
    nTeach: function() {
        var sum = 0;
        Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {teaches: 1}}).forEach(function(a) {
            sum += a.teaches.length;
        });

        return sum
    }
});

Template.displayProfessorTeachesCoursesOfInstance.events({
    'click #addProfessorTeachesCourseRow': function() {
        var $template = $('.editionRow[professorTeachesCourseEdition]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#professor").val("");
        $newLine.find("#course").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click .remResourceRow': function() {
        Professors.update({_id: this.sId}, {$pull: {teaches: this.cId}});
    },
    'click .remEditionRow': function(e) {
        if ($('.editionRow[professorTeachesCourseEdition]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        }
    },
    'click #submitProfessorTeachesCourses': function() {

        _.map($('.resource-row[professorTeachesCourseEdition]'), function(e){
            if ($(e).find('#professor').val() !== "" && $(e).find('#course').val() !== "")
                Professors.update({ _id: $(e).find('#professor').val()}, {
                    $addToSet: {teaches: $(e).find('#course').val()}
                });
        });

        _.map($('.editionRow[professorTeachesCourseEdition]'), function(e) {

            if ($('.editionRow[professorTeachesCourseEdition]').length > 1)
                $(e).remove();
            else {
                $(e).find('#professor').val("");
                $(e).find('#course').val("");
            }
        });
    },
    'click #loadNext': function() {

        var nProfs = Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();;
        var sum = 0;
        Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {teaches: 1}}).forEach(function(a) {
            sum += a.teaches.length;
        });
        var avgenrol = Math.round(sum / nProfs);

        var stepper = Math.round(nResourcesPerPage /avgenrol);

        resourcePosition = Math.min(
            Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count() - nResourcesPerPage,
            resourcePosition + stepper);
        resourcePositionDep.changed()
    },
    'click #loadPrevious': function() {

        var nProfs = Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();;
        var sum = 0;
        Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {teaches: 1}}).forEach(function(a) {
            sum += a.teaches.length;
        });
        var avgenrol = Math.round(sum / nProfs);

        var stepper = Math.round(nResourcesPerPage /avgenrol);

        resourcePosition = Math.max(0,
            resourcePosition - stepper);
        resourcePositionDep.changed()
    },
    'keyup #searchResource': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200)
});

Template.displayProfessorTeachesCourseOfInstance.helpers({
    disabled: function() {
        return (Session.get('editing')) ? "" : "disabled";
    }
});

Template.editProfessorTeachesCourseOfInstance.helpers({
    professors: function() {
        return Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    },
    courses: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    }
});