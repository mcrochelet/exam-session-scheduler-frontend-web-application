var resourcePosition = 0;
var resourcePositionDep = new Tracker.Dependency();

var nResourcesPerPage = 20;

var searchText = "";
var searchTextDep = new Tracker.Dependency;

Template.displayStudentsOfInstance.helpers({
    editing: function() {
        return Session.get('editing');
    },
    students: function() {
        resourcePositionDep.depend();
        searchTextDep.depend();
        var regex = ".*"+searchText+".*";

        return Students.find(
            {$and: [
                {$or: [
                    {name: {$regex: regex}},
                    {surname: {$regex: regex}}]},
                {instanceId: Session.get('currentInstance')},
                {userId: Meteor.userId()}
            ]},
            {skip: resourcePosition, limit: resourcePosition+nResourcesPerPage, sort: {name: 1, surname: 1}})
    },
    st: function() {
        resourcePositionDep.depend();
        return resourcePosition
    },
    en: function() {
        resourcePositionDep.depend();
        return resourcePosition + nResourcesPerPage
    },
    nStudents: function() {
        return Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
    }

});

Template.displayStudentsOfInstance.events({
    'click #addStudentRow': function() {
        var $template = $('.editionRow[studentEdition]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#name").val("");
        $newLine.find("#surname").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click .remResourceRow': function() {
        Students.remove({_id: this._id});
    },
    'click .remEditionRow': function(e) {
        if ($('.editionRow[studentEdition]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        }
    },
    'click #submitStudents': function() {
        var self = this;

        _.map($('.resource-row[student]'), function(e){
            if ($(e).find('#name').val() !== "" && $(e).find('#surname').val() !== "")
                Students.update({_id: $(e).attr('appId')}, {
                    name: $(e).find('#name').val(),
                    surname: $(e).find('#surname').val(),
                    instanceId: self._id,
                    userId: Meteor.userId()
                });
        });

        _.map($('.resource-row[studentEdition]'), function(e){
            if ($(e).find('#name').val() !== "" && $(e).find('#surname').val() !== ""){
                var candidate = {
                    name: $(e).find('#name').val(),
                    surname: $(e).find('#surname').val(),
                    instanceId: self._id,
                    userId: Meteor.userId()
                };
                if (!Students.findOne(candidate)) Students.insert(candidate);
            }
        });

        _.map($('.editionRow[studentEdition]'), function(e) {

            if ($('.editionRow[studentEdition]').length > 1)
                $(e).remove();
            else {
                $(e).find('#name').val("");
                $(e).find('#surname').val("");
            }
        });
    },
    'click #loadNext': function() {
        resourcePosition = Math.min(
            Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count() - nResourcesPerPage,
            resourcePosition + nResourcesPerPage);
        resourcePositionDep.changed()
    },
    'click #loadPrevious': function() {
        resourcePosition = Math.max(0,
            resourcePosition - nResourcesPerPage);
        resourcePositionDep.changed()
    },
    'keyup #searchResource': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200)
});

Template.displayStudentOfInstance.helpers({
    disabled: function() {
        return (Session.get('editing')) ? "" : "disabled";
    }
});