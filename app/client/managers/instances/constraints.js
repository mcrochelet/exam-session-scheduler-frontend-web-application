Template.displayConstraintsOfInstance.helpers({
    editing: function() {
        return Session.get('editing');
    },
    constraints: function() {
        return _.map(Constraints.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).fetch(),
            function(constraint){
                var resource = (constraint.constraintType === 'prof-unavailable')
                    ? Professors.findOne({_id: constraint.profId})
                    : Courses.findOne({_id: constraint.courseId});

                var resourceName = (constraint.constraintType === 'prof-unavailable')
                    ? resource.name + ', '+ resource.surname
                    : resource.tag;

                return {
                    _id: constraint._id,
                    constraintType: constraint.constraintType,
                    resourceName:  resourceName ,
                    resourceId: resource._id,
                    slots: (constraint.slots) ? constraint.slots.join(', ') : undefined,
                    nSlots: (constraint.nSlots) ? constraint.nSlots : undefined,
                    nStudentsPerSlot: (constraint.nStudentsPerSlot) ? constraint.nStudentsPerSlot : undefined
                }
            });
    }
});
Template.displayConstraintsOfInstance.events({
    'click #addConstraintFixedRow': function() {
        var $template = $('.editionRow[ConstraintFixed]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#resource").val("");
        $newLine.find("#slots").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click #addConstraintUnavailableRow': function() {
        var $template = $('.editionRow[ConstraintUnavailable]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#resource").val("");
        $newLine.find("#slots").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click #addConstraintOralRow': function() {
        var $template = $('.editionRow[ConstraintOral]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#resource").val("");
        $newLine.find("#nSlots").val("");
        $newLine.find("#nStudentsPerSlot").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click #addConstraintMultislotRow': function() {
        var $template = $('.editionRow[ConstraintMultislot]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#resource").val("");
        $newLine.find("#nSlots").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click #addConstraintProfUnavailableRow': function() {
        var $template = $('.editionRow[ConstraintProfUnavailable]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#resource").val("");
        $newLine.find("#slots").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click .remEditionRow': function(e) {
        var $row = $(e.target).parents('.editionRow');

        if ($row.attr('ConstraintFixed') === "" && $('.editionRow[ConstraintFixed]').length > 1) {
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        } else if ($row.attr('ConstraintUnavailable') === "" && $('.editionRow[ConstraintUnavailable]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        } else if ($row.attr('ConstraintOral') === "" && $('.editionRow[ConstraintOral]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        } else if ($row.attr('ConstraintMultislot') === "" && $('.editionRow[ConstraintMultislot]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        } else if ($row.attr('ConstraintProfUnavailable') === "" && $('.editionRow[ConstraintProfUnavailable]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        }
    },
    'click #submitConstraints': function() {

        _.map($('.resource-row[ConstraintFixed]'), function(e){
            if ($(e).find('#resource').val() !== "" && $(e).find('#slots').val() !== ""){
                var candidate = {
                    instanceId: Session.get('currentInstance'),
                    userId: Meteor.userId(),
                    constraintType: 'fixed',
                    courseId: $(e).find('#resource').val(),
                    slots: $(e).find('#slots').val().split(', ')
                };
                if (!Constraints.findOne(candidate)) Constraints.insert(candidate)
            }
        });
        _.map($('.resource-row[ConstraintUnavailable]'), function(e){
            if ($(e).find('#resource').val() !== "" && $(e).find('#slots').val() !== "") {
                var candidate = {
                    instanceId: Session.get('currentInstance'),
                    userId: Meteor.userId(),
                    constraintType: 'unavailable',
                    courseId: $(e).find('#resource').val(),
                    slots: $(e).find('#slots').val().split(', ')
                };
                if (!Constraints.findOne(candidate)) Constraints.insert(candidate)
            }
        });
        _.map($('.resource-row[ConstraintOral]'), function(e){
            if ($(e).find('#resource').val() !== "" && $(e).find('#nSlots').val() !== "" && $(e).find('#nStudentsPerSlot').val() !== ""){
                var candidate = {
                    instanceId: Session.get('currentInstance'),
                    userId: Meteor.userId(),
                    constraintType: 'oral',
                    courseId: $(e).find('#resource').val(),
                    nSlots: $(e).find('#nSlots').val(),
                    nStudentsPerSlot: $(e).find('#nStudentsPerSlot').val()
                };
                if (!Constraints.findOne(candidate)) Constraints.insert(candidate)
            }
        });
        _.map($('.resource-row[ConstraintMultislot]'), function(e){
            if ($(e).find('#resource').val() !== "" && $(e).find('#nSlots').val() !== ""){
                var candidate = {
                    instanceId: Session.get('currentInstance'),
                    userId: Meteor.userId(),
                    constraintType: 'multislot',
                    courseId: $(e).find('#resource').val(),
                    nSlots: $(e).find('#nSlots').val()
                };
                if (!Constraints.findOne(candidate)) Constraints.insert(candidate)
            }
        });
        _.map($('.resource-row[ConstraintProfUnavailable]'), function(e){
            if ($(e).find('#resource').val() !== "" && $(e).find('#slots').val() !== ""){
                var candidate = {
                    instanceId: Session.get('currentInstance'),
                    userId: Meteor.userId(),
                    constraintType: 'prof-unavailable',
                    profId: $(e).find('#resource').val(),
                    slots: $(e).find('#slots').val().split(', ')
                };
                if (!Constraints.findOne(candidate)) Constraints.insert(candidate)
            }
        });

        _.map($('.editionRow[ConstraintFixed]'), function(e) {
            if ($('.editionRow[ConstraintFixed]').length > 1)
                $(e).remove();
            else {
                $(e).find('#slots').val("");
                $(e).find('#resource').val("");
            }
        });
        _.map($('.editionRow[ConstraintUnavailable]'), function(e) {
            if ($('.editionRow[ConstraintUnavailable]').length > 1)
                $(e).remove();
            else {
                $(e).find('#slots').val("");
                $(e).find('#resource').val("");
            }
        });
        _.map($('.editionRow[ConstraintOral]'), function(e) {
            if ($('.editionRow[ConstraintOral]').length > 1)
                $(e).remove();
            else {
                $(e).find('#nSlots').val("");
                $(e).find('#nStudentsPerSlot').val("");
                $(e).find('#resource').val("");
            }
        });
        _.map($('.editionRow[ConstraintMultislot]'), function(e) {
            if ($('.editionRow[ConstraintMultislot]').length > 1)
                $(e).remove();
            else {
                $(e).find('#nSlots').val("");
                $(e).find('#resource').val("");
            }
        });
        _.map($('.editionRow[ConstraintProfUnavailable]'), function(e) {
            if ($('.editionRow[ConstraintProfUnavailable]').length > 1)
                $(e).remove();
            else {
                $(e).find('#slots').val("");
                $(e).find('#resource').val("");
            }
        });
    }
});


Template.displayConstraintOfInstance.helpers({
    disabled: function() {
        return (Session.get('editing')) ? "" : "disabled";
    }
});
Template.displayConstraintOfInstance.events({
    'click .remResourceRow': function() {
        Constraints.remove({_id: this._id});
    }
});

Template.editConstraintFixedOfInstance.helpers({
    possibleResource: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    }
});

Template.editConstraintUnavailableOfInstance.helpers({
    possibleResource: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    }
});

Template.editConstraintOralOfInstance.helpers({
    possibleResource: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    }
});

Template.editConstraintMultislotOfInstance.helpers({
    possibleResource: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    }
});

Template.editConstraintProfUnavailableOfInstance.helpers({
    possibleResource: function() {
        return Professors.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()})
    }
});
