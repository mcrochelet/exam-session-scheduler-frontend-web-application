var searchText = "";
var searchTextDep = new Tracker.Dependency;

var openedYear = "";
var openedYearDep = new Tracker.Dependency;

var displayAlert = false;
var displayAlertDep = new Tracker.Dependency;

Template.instances.helpers({
    matchingInstances: function() {
        searchTextDep.depend();
        var currentInstance = Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()});
        var regex = ".*"+searchText+".*";
        var matches = Instances.find({$and: [{$or: [{year: {$regex: regex}}, {month: {$regex: regex}}]}, {userId: Meteor.userId()}]}).fetch();
        if (currentInstance) matches = _.filter(matches, function(doc) {return (doc.year !== currentInstance.year)
            || (doc.month !== currentInstance.month)});
        var groupedResults = _(matches).groupBy(function(doc) {return doc.year});
        groupedResults = _(groupedResults).pairs();

        groupedResults = _(groupedResults).map(function(array) {
            var docs = _.map(array[1], function(doc) {return {month: doc.month, _id: doc._id}});
            return {
                year: array[0],
                docs: docs
            }
        });
        return _(groupedResults).sortBy(function(doc) {return doc.year});

    },
    currentInstance: function() {
        return Instances.findOne({_id: Session.get('currentInstance')});
    }
});

Template.instances.events({
    'keyup #search': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200),
    'click #removeInstance': function() {
        Instances.remove({_id: this._id});

        Students.find({userId: Meteor.userId(), instanceId: this._id}).forEach(function(s){
            Students.remove({_id: s._id});
        });
        Courses.find({userId: Meteor.userId(), instanceId: this._id}).forEach(function(s){
            Courses.remove({_id: s._id});
        });
        Professors.find({userId: Meteor.userId(), instanceId: this._id}).forEach(function(s){
            Professors.remove({_id: s._id});
        });
        Constraints.find({userId: Meteor.userId(), instanceId: this._id}).forEach(function(s){
            Constraints.remove({_id: s._id});
        });


        Session.set('currentInstance', "");
        Session.set('editing', false);
        $('#removalModal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    },
    'click #edit': function() {
        if (Session.get("editing")) {
            _.map($('.editionRow'), function(e) {
                $(e).velocity("transition.fadeOut", {
                    complete: function() { $(e).remove();}
                });
            });
            Session.set("editing", false);
        } else Session.set("editing", true);
    }

});

Template.displaySearchResult.events({
    'click .year-level': function() {
        if (openedYear !== this.year) openedYear = this.year;
        else openedYear = "";
        openedYearDep.changed();
    }
});

Template.displaySearchResult.helpers({
    isOpen: function() {
        openedYearDep.depend();
        return this.year === openedYear
    }
});

Template.displayMonthResult.events({
    'click .month-level': function() {
        Session.set('currentInstance', this._id);
        Session.set('editing', false);
        _.map($('.editionRow'), function(e) {
            $(e).velocity("transition.fadeOut", {
                complete: function() { $(e).remove();}
            });
        });
    }
});

Template.displayInstanceCreationRow.helpers({
    possibleMonth: function() {
        return [{month:"January"}, {month:"June"}, {month:"September"}];
    },
    displayAlert: function() {
        displayAlertDep.depend();
        return displayAlert
    }
});

Template.displayInstanceCreationRow.events({
    'click #addInstance': function() {

        var month = $('#monthPicker').val();

        var y1 = parseInt($('#year1').val());
        var y2 = parseInt($('#year2').val());

        if (!isNaN(y1) && !isNaN(y2) && (y2-y1) === 1) {
            var year = y1 + " - " + y2;
            var candidateInstance = {month: month, year: year, userId: Meteor.userId(), status:'idle'};

            if (!Instances.findOne(candidateInstance)) {
                var id = Instances.insert(candidateInstance);
                $('#creationModal').modal('hide');
                Session.set('currentInstance', id);
                Session.set('editing', false);
                displayAlert = false;
                displayAlertDep.changed()
            } else {
                displayAlert = true;
                displayAlertDep.changed()
                $('#creationModalLabelAlert').velocity('callout.bounce');
            }
        } else {

        }
    },
    'input #year1': function(){
        $('#year2').val(parseInt($('#year1').val())+1);
    }
});
