var resourcePosition = 0;
var resourcePositionDep = new Tracker.Dependency();

var nResourcesPerPage = 20;

var searchText = "";
var searchTextDep = new Tracker.Dependency;

Template.displayStudentFollowsCoursesOfInstance.helpers({
    editing: function() {
        return Session.get('editing');
    },
    links: function() {
        var results = [];

        resourcePositionDep.depend();
        searchTextDep.depend();
        var regex = ".*"+searchText+".*";

        var sum = 0;
        Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {follows: 1}}).forEach(function(a) {
            sum += a.follows.length;
        });
        var nStud = Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
        var avgenrol = Math.round(sum / nStud);

        var stepper = Math.round(nResourcesPerPage /avgenrol);

        _.map(Students.find({$and: [
            {$or: [
                {name: {$regex: regex}},
                {surname: {$regex: regex}}]},
            {instanceId: Session.get('currentInstance')},
            {userId: Meteor.userId()}
        ]},  {
            skip: resourcePosition,
            limit: resourcePosition+stepper,
            sort: {name: 1, surname: 1}}).fetch(), function(s) {
            if (s && s.follows && s.name) _.map(s.follows, function(cId) {
                var course = Courses.findOne({_id: cId});
                if (course) results.push({
                    sId: s._id,
                    name: s.name,
                    surname:s.surname,
                    cId: course._id,
                    courseTag: course.tag,
                    courseName: course.name
                });
            });
        });
        return results
    },
    nEnrol: function() {
        var sum = 0;
        Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {follows: 1}}).forEach(function(a) {
            sum += a.follows.length;
        });
        return sum;
    }

});

Template.displayStudentFollowsCoursesOfInstance.events({
    'click #addStudentFollowsCourseRow': function() {
        var $template = $('.editionRow[studentFollowsCourseEdition]:last-of-type');
        var $newLine = $template.clone(true);
        $newLine.find("#student").val("");
        $newLine.find("#course").val("");
        $newLine.velocity("transition.slideLeftIn");
        $template.after($newLine);
    },
    'click .remResourceRow': function() {
        Students.update({_id: this.sId}, {$pull: {follows: this.cId}});
    },
    'click .remEditionRow': function(e) {
        if ($('.editionRow[studentFollowsCourseEdition]').length > 1) {
            var $row = $(e.target).parents('.editionRow');
            $row.velocity("transition.slideLeftOut", {
                complete: function() { $row.remove();}
            });
        }
    },
    'click #submitStudentFollowsCourses': function() {

        _.map($('.resource-row[studentFollowsCourseEdition]'), function(e){
            if ($(e).find('#student').val() !== "" && $(e).find('#course').val() !== "")
                Students.update({ _id: $(e).find('#student').val()}, {
                    $addToSet: {follows: $(e).find('#course').val()}
                });
        });

        _.map($('.editionRow[studentFollowsCourseEdition]'), function(e) {

            if ($('.editionRow[studentFollowsCourseEdition]').length > 1)
                $(e).remove();
            else {
                $(e).find('#student').val("");
                $(e).find('#course').val("");
            }
        });
    },
    'click #loadNext': function() {

        var sum = 0;
        Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {follows: 1}}).forEach(function(a) {
            sum += a.follows.length;
        });
        var nStud = Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
        var avgenrol = Math.round(sum / nStud);

        var stepper = Math.round(nResourcesPerPage /avgenrol);

        resourcePosition = Math.min(
            Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count() - nResourcesPerPage,
            resourcePosition + stepper);
        resourcePositionDep.changed()
    },
    'click #loadPrevious': function() {

        var sum = 0;
        Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}, {fields: {follows: 1}}).forEach(function(a) {
            sum += a.follows.length;
        });
        var nStud = Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()}).count();
        var avgenrol = Math.round(sum / nStud);

        var stepper = Math.round(nResourcesPerPage /avgenrol);

        resourcePosition = Math.max(0,
            resourcePosition - stepper);
        resourcePositionDep.changed()
    },
    'keyup #searchResource': _.throttle(function(e) {
        if ($(e.target).val().trim() !== searchText) {
            searchText = $(e.target).val().trim();
            searchTextDep.changed();
        }
    }, 200)
});

Template.displayStudentFollowsCourseOfInstance.helpers({
    disabled: function() {
        return (Session.get('editing')) ? "" : "disabled";
    }
});

Template.editStudentFollowsCourseOfInstance.helpers({
    students: function() {
        return Students.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    },
    courses: function() {
        return Courses.find({instanceId: Session.get('currentInstance'), userId: Meteor.userId()});
    }
});