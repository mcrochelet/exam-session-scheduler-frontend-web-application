var searchText = "";
var searchTextDep = new Tracker.Dependency;

Template.statistics.helpers({
	matchingInstances: function() {
		searchTextDep.depend();
		var currentInstance = Instances.findOne({_id: Session.get('currentInstance'), userId: Meteor.userId()});
		var regex = ".*"+searchText+".*";
		var matches = Instances.find({$and: [{$or: [{year: {$regex: regex}}, {month: {$regex: regex}}]}, {userId: Meteor.userId()}]}).fetch();
		if (currentInstance) matches = _.filter(matches, function(doc) {return (doc.year !== currentInstance.year)
			|| (doc.month !== currentInstance.month)});
		var groupedResults = _(matches).groupBy(function(doc) {return doc.year});
		groupedResults = _(groupedResults).pairs();

		groupedResults = _(groupedResults).map(function(array) {
			var docs = _.map(array[1], function(doc) {return {month: doc.month, _id: doc._id}});
			return {
				year: array[0],
				docs: docs
			}
		});
		return _(groupedResults).sortBy(function(doc) {return doc.year});

	},
	currentInstance: function() {
		return Instances.findOne({_id: Session.get('currentInstance')});
	}
});

Template.statistics.events({
	'keyup #search': _.throttle(function(e) {
		if ($(e.target).val().trim() !== searchText) {
			searchText = $(e.target).val().trim();
			searchTextDep.changed();
		}
	}, 200)
});