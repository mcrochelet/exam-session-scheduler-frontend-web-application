/**
 * Created by mcrochelet on 20/05/15.
 */

Template.navigation.events = {
    'click #sidebarButton': function() {
        $('.ui.sidebar')
            .sidebar('setting', 'transition', 'overlay')
            .sidebar('show', true);
    },
    'click #modalButton': function () {
        if (Meteor.user() === null) {
            $('#loginModal').modal('show');
        } else {
            Meteor.logout();
        }
    }
};

Template.navigation.helpers({
    loggedIn: function() {
        return Meteor.user() !== null
    }
})