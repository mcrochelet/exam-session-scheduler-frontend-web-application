var drawchart = function() {
    var $chart = d3.select('#barPlot svg');

    var r = Results.findOne({instanceId: Session.get('currentInstance')});

    var nStudents = Students.find({userId: Meteor.userId(), instanceId: Session.get('currentInstance')}).count()
    var data = {
        key: 'stats',
        values: []
    };

    if (r && r.stats) {
        _.map(r.stats, function(a) {
            data.values.push({nDays: a[0], nStud: a[1]})
        });
    } else for (var i=0; i< 24; i++) {
        data.values.push({nDays: i, nStud: 0})
    }

    nv.addGraph(function() {
        var chart = nv.models.discreteBarChart()
            .x(function(d) { return d.nDays })
            .y(function(d) { return d.nStud })
            .margin({
                top:    50,
                right:  50,
                bottom: 100,
                left:   100})
            .staggerLabels(false)
            .tooltips(true)
            .showValues(false)
            .showYAxis(true)
            .showXAxis(true)
            .forceY([0, nStudents])

        chart.yAxis
            .axisLabel('Number of Students')

        chart.xAxis
            .axisLabel('Minimum number of days between consecutive exams');

        $chart
            .datum([data])
            .transition().duration(500)
            .call(chart)
        ;

        nv.utils.windowResize(chart.update);

        return chart;
    });


    return ""
};

Template.barPlot.helpers({
    renderBarPlot: drawchart
});

Template.barPlot.rendered = drawchart;