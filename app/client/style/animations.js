
var timingDownAndUp = 500;

Momentum.registerPlugin('downAndUp', function(options) {
    return {
        insertElement: function(node, next) {
            var $next = $(next);
            var $node = $(node).insertBefore($next);

            var nodeid = $node.attr('id');

            if (nodeid === 'landing')
                $node.velocity('fadeIn', { delay: timingDownAndUp-100, duration: timingDownAndUp });
            else
                $node.velocity({ translateY: ["0%", "100%"] }, { delay: timingDownAndUp-100, duration: timingDownAndUp });
        },
        removeElement: function(node) {
            var $node = $(node);
            var nodeid = $node.attr('id');
            if (nodeid === 'landing')
                $(node)
                    .velocity("fadeOut", {
                        duration: timingDownAndUp,
                        complete: function() {
                            $(node).remove()
                        }
                    });
            else
                $(node)
                    .velocity({ translateY: ["100%", "0%"] }, {
                        duration: timingDownAndUp,
                        complete: function() {
                            $(node).remove()
                        }
                    });

        }
    }
});


var timingSwitchPage = 300;
Momentum.registerPlugin('switchPage', function(options) {
    return {
        insertElement: function(node, next) {
            var $next = $(next);


            var $fromto = $('#fromto');

            var from = $fromto.attr('from');
            var to = $fromto.attr('to');

            if (to < from) {
                $(node).css('transform:translateX(-100%);');
                var $node = $(node).insertBefore($next);
                $node.velocity({ translateX: ["0%", "-100%"] }, {duration: timingSwitchPage+100, begin: function() {
                    $node.css('transform:translateX(0%);')
                } });
            }
            else {
                var $node = $(node).insertBefore($next);
                $node.velocity({ translateX: ["0%", "100%"] }, { delay: timingSwitchPage-100, duration: timingSwitchPage });
            }
        },
        removeElement: function(node) {

            var $fromto = $('#fromto');

            var from = $fromto.attr('from');
            var to = $fromto.attr('to');

            if (to < from)
                $(node)
                    .velocity({ translateX: ["100%", "0%"] }, {
                        duration: timingSwitchPage,
                        complete: function() {
                            $(node).remove()
                        }
                    });
            else
                $(node)
                    .velocity({ translateX: ["-100%", "0%"] }, {
                        duration: timingSwitchPage,
                        complete: function() {
                            $(node).remove()
                        }
                    });

        }
    }
});