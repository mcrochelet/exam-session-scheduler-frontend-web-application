/**
 * Created by mcrochelet on 20/05/15.
 * Defines the (client side) routes handled by the application.
 * Only renders the corresponding template when all the needed data are cached locally and display the loading template
 * while waiting
 */
Router.configure({
    layoutTemplate: 'layout',
    loadingTemplate: 'loading'
});

// Enforce User login before leaving the landingPage.
Router.onBeforeAction(function () {
    if (!Meteor.userId()) {
        this.render('landingPage');
    } else {
        this.next();
    }
});

landing = Router.route('/', {
    name: 'landingPage'
});

aboutPage = Router.route('/about', {
    name: 'about'
});

instancesPage = Router.route('/instances', {
    name: 'instances',
    onStop: function() {
        Session.set('editing', false);
    },
    waitOn: function() {
        return [Meteor.subscribe('instances'),
            Meteor.subscribe('students'),
            Meteor.subscribe('courses'),
            Meteor.subscribe('professors'),
            Meteor.subscribe('constraints'),
            Meteor.subscribe('bulk-files'),
            Meteor.subscribe('results')]
    }
});

resultsPage = Router.route('/results', {
    name: 'results',
    waitOn: function() {
        return [Meteor.subscribe('instances'),
            Meteor.subscribe('students'),
            Meteor.subscribe('courses'),
            Meteor.subscribe('professors'),
            Meteor.subscribe('constraints'),
            Meteor.subscribe('bulk-files'),
            Meteor.subscribe('results')]
    }
});

statisticsPage = Router.route('/statistics', {
    name: 'statistics',
    waitOn: function() {
        return [Meteor.subscribe('instances'),
            Meteor.subscribe('students'),
            Meteor.subscribe('courses'),
            Meteor.subscribe('professors'),
            Meteor.subscribe('constraints'),
            Meteor.subscribe('bulk-files'),
            Meteor.subscribe('results')]
    }
});
