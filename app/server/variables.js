/**
 * Globally defined variables for the server
 */

solverAddress = 'http://localhost:8080';
maxTimeout = 5*1000; //ms
myAddress = 'localhost';
myPort = 3000;
myWebHookRelativePath = '/api/solution';
debugging = false;