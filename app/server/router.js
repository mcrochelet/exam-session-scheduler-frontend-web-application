/**
 * Webhook definition:
 * Define a webhook address pattern: /api/solution/:instanceId
 * Each time a request is POST'ed (or PUT'ed or DELETE'd for that matters, there is no filter on the request method,
 * only that it must containt a (JSON parsable) content) the solution is stored in the Results collection with
 * the instanceId and userId set accordingly
 */

HTTP.methods({
    '/api/solution/:instanceId': function(data) {
        var result = JSON.parse(data);
        var instanceId = this.params.instanceId.replace(':', '');
        var cinst = Instances.findOne({_id: instanceId});
        if (cinst && result)
            Results.upsert({
                instanceId: instanceId
            },{ $set:{
                instanceId: instanceId,
                userId: cinst.userId,
                results: result.solution,
                stats: result.stat
            }});
    }
});