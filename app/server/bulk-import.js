/**
 * Detects the addition of a new document (of status 'to-parse') in the bulkFiles collection and inflate this document
 * into a full blown instance (creates corresponding students, courses, professors, enrolment links, etc.)
 */
BulkFiles.find({status: 'to-parse'}).observe({
    added: function(file) {

        var instanceId = file.instanceId;
        var userId = file.userId;
        var results = file.results;

        var studs = {};
        var cours = {};
        var profs = {};

        if (results && results.meta && results.data) {
            var headers = _.filter(results.meta.fields, function (h) {
                return h !== 'null'
            });
            _.map(results.data, function (row) {
                row = _.omit(row, 'null');
                if (row.inscribed === 'I' || row.inscribed ==='S') {
                    var s = row.student.split(', ').map(function (n) {
                        return n.trim()
                    });
                    var c = row.course.split(', ').map(function (n) {
                        return n.trim()
                    });
                    var praw = row.profs.split(', ').map(function (n) {
                        return n.trim()
                    });
                    var p = praw.map(function (pr) {
                        var p = pr.split('-').map(function (n) {
                            return n.trim()
                        });
                        if (p.length > 1) return {name: p[0], surname: p[1]}
                        else return {name: p[0]}
                    });

                    if (!studs[row.student]) {
                        var candidate = {
                            userId: userId,
                            instanceId: instanceId
                        };

                        if (s.length > 1) {
                            candidate['name'] = s[0];
                            candidate['surname'] = s[1];
                        } else candidate['name'] = s[0];

                        headers.map(function (h) {
                            if (h !== 'student' && h.indexOf('stud') > -1) candidate[h] = row[h]
                        });
                        if (candidate.name)
                            if (!Students.findOne(candidate))
                                studs[row.student] = Students.insert(candidate);
                            else studs[row.student] = Students.findOne(candidate)._id;
                    }

                    if (!cours[row.course]) {
                        var candidate = {
                            userId: userId,
                            instanceId: instanceId
                        };

                        if (c.length > 1) {
                            candidate['tag'] = c[0];
                            candidate['name'] = c[1];
                        } else candidate['tag'] = c[0];

                        if (candidate.tag)
                            if (!Courses.findOne(candidate))
                                cours[row.course] = Courses.insert(candidate);
                            else cours[row.course] = Courses.findOne(candidate)._id;
                    }
                    for (var i = 0; i < p.length; i++) {
                        var index = p[i].name;
                        if (p[i].surname) index = index + " - " + p[i].surname;
                        if (!profs[index]) {
                            var candidate = {
                                userId: userId,
                                instanceId: instanceId
                            };

                            if (p[i].surname) {
                                candidate['name'] = p[i].name;
                                candidate['surname'] = p[i].surname;
                            } else candidate['name'] = p[i].name;

                            if (candidate.name)
                                if (!Professors.findOne(candidate))
                                    profs[index] = Professors.insert(candidate);
                                else profs[index] = Professors.findOne(candidate)._id;
                        }
                        Professors.update({_id: profs[index]}, {
                            $addToSet: {teaches: cours[row.course]}
                        })
                    }

                    Students.update({_id: studs[row.student]}, {
                        $addToSet: {follows: cours[row.course]}
                    })

                }
            });
        }

        BulkFiles.remove(file);
    }
});