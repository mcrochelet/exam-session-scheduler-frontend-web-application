/**
 * Defines the method solve which is used to communicate with the REST interface
 * If the instance is idle, sends all the instance data to the interface and starts the search (the instance status
 * is moved respectively to syncing and then to solving)
 * The method defines a timeout at the end of which the instance is moved back to the idle status and the REST
 * corresponding instance is destroyed
 *
 * Note: the current behaviour is not  very safe against meteor server restarts (an instance could potentially be locked
 *       in the solving (or syncing) statuses
 */

Meteor.methods({
    solve: function (instanceId, maxTime) {

        // a bit of sugar never hurts anyone (except in coffee, that's sacrilege)
        if (!Array.prototype.last){
            Array.prototype.last = function(){
                return this[this.length - 1];
            };
        }

        var studs = {};
        var cours = {};
        var profs = {};


        var instance = Instances.findOne({userId: Meteor.userId(), _id: instanceId});
        var instanceNameDefined = false;
        if (instance.status === 'idle') {
            try {
                var r = HTTP.get(solverAddress+'/instance', {timeout: maxTimeout});
                if (r.statusCode  >= 299) throw new Meteor.Error("solver server error", "get instance name");
                var instanceName = r.content;
                instanceNameDefined = true;

                Instances.update({_id: instanceId}, {$set: {
                    status: 'syncing'
                }});

                if (debugging) console.log(instanceName); // for debug


                //var momentMondayIndex = moment().day("Monday").day();
                var firstDay = moment(instance.start).day(); // on the server, moment locale is correct.
                /*if (momentMondayIndex !== 0) {
                 firstDay= firstDay-momentMondayIndex;
                 firstDay = (firstDay === -1)? 6 : firstDay;
                 }*/
                var numberOfDays = moment(instance.end).diff(moment(instance.start), 'days') + 1;
                var nSessionPerDay = instance.nSessionsPerDay;

                r = HTTP.post(solverAddress+'/instance/session', {
                    headers: {
                        'X-instance-name': instanceName
                    },
                    timeout: maxTimeout,
                    data: {
                        nDays: numberOfDays,
                        nSessionPerDay: nSessionPerDay,
                        firstDay: firstDay
                    }
                });
                if (r.statusCode  >= 299) throw new Meteor.Error("solver server error", "set session days");

                if (debugging) console.log('sending courses');
                Courses.find({userId: Meteor.userId(), instanceId: instanceId}).forEach(function(course) {
                    r = HTTP.post(solverAddress+'/course', {
                        headers: {
                            'X-instance-name': instanceName
                        },
                        timeout: maxTimeout,
                        data: {
                            name: course.tag
                        }
                    });
                    if (r.statusCode  >= 299) throw new Meteor.Error("solver server error", "set course "+course.tag);
                    var cId = r.content.split('/').last();
                    cours[course._id] = cId;

                });
                if (debugging) console.log('sending students and follows links');
                Students.find({userId: Meteor.userId(), instanceId: instanceId}).forEach(function(student) {
                    r = HTTP.post(solverAddress + '/student', {
                        headers: {
                            'X-instance-name': instanceName
                        },
                        timeout: maxTimeout,
                        data: {
                            name: student.name + ', ' + student.surname
                        }
                    });
                    if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "set student " + student.name + ', ' + student.surname);
                    var sId = r.content.split('/').last();
                    studs[student._id] = sId;
                    _.map(student.follows, function(cId) {
                        r = HTTP.post(solverAddress + '/student/'+sId+'/course/'+cours[cId], {
                            headers: {
                                'X-instance-name': instanceName
                            },
                            timeout: maxTimeout
                        });
                        if (r.statusCode  >= 299) throw new Meteor.Error("solver server error", "set student"+student.name +"follows course"+ Courses.findOne({_id: cId}));
                    })
                });
                if (debugging) console.log('sending professors and teaches links');

                Professors.find({userId: Meteor.userId(), instanceId: instanceId}).forEach(function(professor) {
                    r = HTTP.post(solverAddress + '/professor', {
                        headers: {
                            'X-instance-name': instanceName
                        },
                        timeout: maxTimeout,
                        data: {
                            name: professor.name + ', ' + professor.surname
                        }
                    });
                    if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "set professor " + professor.name + ', ' + professor.surname);
                    var pId = r.content.split('/').last();
                    profs[professor._id] = pId;
                    _.map(professor.teaches, function(cId) {
                        r = HTTP.post(solverAddress + '/professor/'+pId+'/course/'+cours[cId], {
                            headers: {
                                'X-instance-name': instanceName
                            },
                            timeout: maxTimeout
                        });
                        if (r.statusCode  >= 299) throw new Meteor.Error("solver server error", "set professor"+professor.name +"teaches course"+ Courses.findOne({_id: cId}));
                    })
                });


                Constraints.find({userId: Meteor.userId(), instanceId: instanceId}).forEach(function(constraint) {
                    var data = {};
                    data['Type'] = constraint.constraintType;
                    var args = [];

                    if (constraint.constraintType !== 'prof-unavailable')
                        args.push(Courses.findOne({_id: constraint.courseId}).tag);
                    else
                        args.push(Professors.findOne({_id: constraint.profId}).tag);

                    if (constraint.constraintType === 'fixed') {
                        args.push.apply(args, constraint.slots);
                    } else if (constraint.constraintType === 'unavailable') {
                        args.push.apply(args, constraint.slots);
                    } else if (constraint.constraintType === 'oral') {
                        args.push(constraint.nSlots);
                        args.push(constraint.nStudentsPerSlot);
                    } else if (constraint.constraintType === 'multislot') {
                        args.push(constraint.nSlots);
                    } else if (constraint.constraintType === 'prof-unavailable') {
                        args.push.apply(args, constraint.slots);
                    }
                    data['args'] = args;
                    r = HTTP.post(solverAddress + '/constraint', {
                        headers: {
                            'X-instance-name': instanceName
                        },
                        timeout: maxTimeout,
                        data: data
                    });
                    if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "set constraint ");
                });

                r = HTTP.post(solverAddress + '/instance/callback', {
                    headers: {
                        'X-instance-name': instanceName
                    },
                    timeout: maxTimeout,
                    data: {
                        address: myAddress,
                        port: myPort,
                        relPath: myWebHookRelativePath+'/:'+instanceId
                    }
                });
                if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "set callback ");

                r = HTTP.post(solverAddress + '/instance/search?searchTime='+(maxTime*60*1000), {
                    headers: {
                        'X-instance-name': instanceName
                    },
                    timeout: maxTimeout
                });
                if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "set callback ");


                Meteor.setTimeout(function() {
                    HTTP.del(solverAddress + '/instance', {
                        content: instanceName,
                        timeout: maxTimeout
                    });
                    if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "impossible to remove solver side instance");
                    Instances.update({_id: instanceId}, {$set: {status: 'idle'}});

                }, maxTime*60*1000 + 120*1000);

                Instances.update({_id: instanceId}, {$set: {status: 'solving'}})
            }catch (e) {
                if (instanceNameDefined)
                    HTTP.del(solverAddress + '/instance', {
                        content: instanceName,
                        timeout: maxTimeout
                    });
                if (r.statusCode >= 299) throw new Meteor.Error("solver server error", "impossible to remove solver side instance");
                Instances.update({_id: instanceId}, {$set: {status: 'idle'}});
            }
        }
    }
});