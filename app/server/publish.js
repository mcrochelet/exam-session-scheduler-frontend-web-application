/**
 * Publishing strategies
 * Currently the publishing strategies are reduced to their simplest expression: only publish data that belong to the user
 */

Meteor.publish('instances', function(){
   return Instances.find({userId: this.userId})
});
Meteor.publish('students', function(){
    return Students.find({userId: this.userId})
});
Meteor.publish('courses', function(){
    return Courses.find({userId: this.userId})
});
Meteor.publish('professors', function(){
    return Professors.find({userId: this.userId})
});
Meteor.publish('constraints', function(){
    return Constraints.find({userId: this.userId})
});
Meteor.publish('bulk-files', function(){
    return BulkFiles.find() // no interest since the BulkFiles are deleted after hitting the server
});
Meteor.publish('results', function(){
    return Results.find({userId: this.userId})
});